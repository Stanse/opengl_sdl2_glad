#version 300 es
precision mediump float;

in vec3 obj_normal;
in vec3 fragment_position;

out vec4 color;

uniform vec3 object_color;
uniform vec3 light_position;
uniform vec3 light_color;
uniform vec3 view_position;

void main()
{
    float ambient_strength = 0.1f;
    vec3 ambient = ambient_strength * light_color;

    vec3 norm = normalize(obj_normal);
    vec3 light_dir = normalize(light_position - fragment_position);
    float diff = max(dot(norm, light_dir), 0.0f);
    vec3 diffuse = diff * light_color;

    float specular_strength = 0.5f;
    vec3 view_dir = normalize(view_position - fragment_position);
    vec3 reflect_dir = reflect(-light_dir, norm);
    float spec = max(dot(view_dir, reflect_dir), 0.0);
    spec = pow(spec, 32.0f);
    vec3 specular = specular_strength * spec * light_color;

    vec3 result = (ambient + diffuse + specular) * object_color;
    color = vec4(result, 1.0f);
}
