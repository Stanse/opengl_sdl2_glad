#version 300 es
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

out vec3 obj_normal;
out vec3 fragment_position;

uniform mat4 model;
uniform mat3 transpose_model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0);
    obj_normal = transpose_model * normal;
//    obj_normal = mat3(transpose(inverse(model))) * normal;  // worse performance
//    obj_normal = normal;
    fragment_position = vec3(model * vec4(position, 1.0f));
}
