#version 320 es
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texture_coords;

out vec3 object_normal;
out vec3 fragment_position;
out vec2 texture_coordinates;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat3 transpose_model;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0f);
    object_normal = transpose_model * normal;
//    object_normal = mat3(transpose(inverse(model))) * normal;  // worse performance
//    object_normal = normal;
    fragment_position = vec3(model * vec4(position, 1.0f));
    texture_coordinates = texture_coords;
}


