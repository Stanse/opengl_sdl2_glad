#include "Shader.h"
#include "Camera.h"
#include <SDL.h>
#include <cassert>

#include "Model.h"

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

const GLint width = 1200;
const GLint height = 1000;
static GLboolean keys[1024]{ false };

GLboolean game_loop = true;
static GLfloat delta_time = 0.0f;
static GLfloat last_frame = 0.0f;
static GLfloat cursor_last_x = 400, cursor_last_y = 300;

static glm::vec3 light_position(-5.0f, 2.0f, 4.0f);

void make_texture(GLuint& texture, const char* path);
void check_user_input(Camera& camera);

int main()
{
  const GLint sdl_init_result = SDL_Init(SDL_INIT_EVERYTHING);
  if (sdl_init_result != 0)
  {
    const char* err_msg{ SDL_GetError() };
    std::cerr << "ERROR: Failed call SDL_Init:\n" << err_msg << std::endl;
    return -1;
  }

  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);
  //  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "16");

  SDL_Window* window = SDL_CreateWindow("OpenGL ES 3.2 || 3.1.0 Assimp Lib", SDL_WINDOWPOS_CENTERED,
                                        SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
  if (!window)
  {
    const char* err_msg{ SDL_GetError() };
    std::cerr << "ERROR: Failed call SDL_CreateWindow" << err_msg << std::endl;
    return -1;
  }

  GLint gl_major_ver = 3;
  GLint gl_minor_ver = 2;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

  SDL_GLContext context{ SDL_GL_CreateContext(window) };

  assert(context != nullptr);

  GLint result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
  assert(result == 0);
  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
  assert(result == 0);

  if (gl_major_ver < 3 && gl_minor_ver < 3)
  {
    std::clog << "current contex openGL version: " << gl_major_ver << '.' << gl_minor_ver << '\n' << std::flush;
    throw std::runtime_error("openGL version too low");
  }

  if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
  {
    std::clog << "ERROR: failed to initialize GLAD" << std::endl;
  }

  Shader shader("Shaders/shader.vs", "Shaders/shader.frag");
  Shader shader_light("Shaders/light_shader.vs", "Shaders/light_shader.frag");

  SDL_Event event;
  Camera camera;
  camera.position = glm::vec3(3.9f, 2.12f, 7.94f);
  camera.yaw = -88;
  camera.pitch = -13.25;
  camera.movement_speed = 10;
  camera.mouse_input(0, 0);

  Model cube("../models/cube/cube.obj");
  Model lamp("../models/lamp/lamp.obj");
  Model floor("../models/floor/floor.obj");
  Model lamp2("../models/planet/planet.obj");

  //      +-------------------------+
  //      |  Global OpenGL settings |
  //      +-------------------------+
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_MULTISAMPLE);  // set MSAA

  //      +-------------------------+
  //      |         Game loop       |
  //      +-------------------------+
  while (game_loop)
  {
    GLfloat current_frame = SDL_GetTicks() * 0.001;
    delta_time = current_frame - last_frame;
    last_frame = current_frame;

    //      +-------------------------+
    //      |         INPUT EVENT     |
    //      +-------------------------+
    while (SDL_PollEvent(&event) != 0)
    {
      if (event.type == SDL_QUIT)
        game_loop = GL_FALSE;
      //  KEYBOARD
      if (event.type == SDL_KEYDOWN)
      {
        if (event.key.keysym.sym >= 0 && event.key.keysym.sym <= 1024)
        {
          keys[event.key.keysym.sym] = GL_TRUE;
        }
      }
      if (event.type == SDL_KEYUP)
      {
        if (event.key.keysym.sym >= 0 && event.key.keysym.sym <= 1024)
        {
          keys[event.key.keysym.sym] = GL_FALSE;
        }
      }
      // MOUSE MOUTION
      if (event.type == SDL_MOUSEMOTION)
      {
        GLint cursor_x_pos;
        GLint cursor_y_pos;
        SDL_GetMouseState(&cursor_x_pos, &cursor_y_pos);

        GLfloat xoffset = cursor_x_pos - cursor_last_x;
        GLfloat yoffset = -cursor_y_pos + cursor_last_y;  // Reversed since y-coordinates go
                                                          // from bottom to left
        cursor_last_x = cursor_x_pos;
        cursor_last_y = cursor_y_pos;

        camera.mouse_input(xoffset, yoffset);
      }
      // MOUSE WHEEL
      if (event.type == SDL_MOUSEWHEEL)
      {
        GLfloat offset = 1;
        if (event.wheel.y > 0)  // scroll up
        {
          if (camera.zoom < 45)
          {
            camera.zoom += offset;
          }
          else
          {
            camera.zoom = 45;
          }
        }
        else if (event.wheel.y < 0)  // scroll down
        {
          if (camera.zoom > 1)
          {
            camera.zoom -= offset;
          }
          else
          {
            camera.zoom = 1;
          }
        }
      }
    }

    glad_glClearColor(.24f, .25f, .25f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    check_user_input(camera);

    //      +-------------------------+
    //      |    CUBE SHADER USE      |
    //      +-------------------------+
    shader.use();

    //    +-------------------------+
    //    |   LIGHT SETTINGS        |
    //    +-------------------------+
    shader.set_float("material.shininess", 64.0f);
    // directional light
    shader.set_vec3("direct_light.direction", -0.2f, -1.0f, -0.3f);
    shader.set_vec3("direct_light.ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("direct_light.diffuse", 0.4f, 0.4f, 0.4f);  // darken the light a bit to fit the scene
    shader.set_vec3("direct_light.specular", 0.5f, 0.5f, 0.5f);

    //    +-------------------------+
    //    |   VIEW                  |
    //    +-------------------------+
    glm::mat4 view;
    view = camera.get_look_at_matrix();

    //    +-------------------------+
    //    |   PROJECTION            |
    //    +-------------------------+
    glm::mat4 projection;
    projection = glm::perspective(camera.zoom, static_cast<GLfloat>(width / height), 0.1f, 1000.0f);

    //    +-------------------------+
    //    |   MODEL                 |
    //    +-------------------------+

    glm::mat3 transpose_model;

    // Point light
    shader.set_vec3("point_lights[0].position", light_position.x, light_position.y, light_position.z);
    shader.set_vec3("point_lights[0].ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("point_lights[0].diffuse", 0.8f, 0.8f, 0.8f);
    shader.set_vec3("point_lights[0].specular", 1.0f, 1.0f, 1.0f);
    shader.set_float("point_lights[0].constant", 1.0f);
    shader.set_float("point_lights[0].linear", 0.09);
    shader.set_float("point_lights[0].quadratic", 0.032);

    glm::mat4 floor_model_pos;
    shader.set_mat4("model", floor_model_pos);
    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(floor_model_pos)));
    shader.set_mat3("transpose_model", transpose_model);
    floor.draw(shader);

    glm::mat4 cube_model_pos;
    cube_model_pos = glm::translate(cube_model_pos, glm::vec3(4.0f, 1.15f, 0.0f));
    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(cube_model_pos)));
    shader.set_vec3("view_pos", camera.position.x, camera.position.y, camera.position.z);
    shader.set_mat4("view", view);
    shader.set_mat4("projection", projection);
    shader.set_mat4("model", cube_model_pos);
    shader.set_mat3("transpose_model", transpose_model);
    cube.draw(shader);

    cube_model_pos = glm::translate(cube_model_pos, glm::vec3(1.2f, 0.0f, -2.3f));
    shader.set_mat4("model", cube_model_pos);
    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(cube_model_pos)));
    shader.set_mat3("transpose_model", transpose_model);
    shader.set_mat4("model", cube_model_pos);
    shader.set_mat3("transpose_model", transpose_model);
    cube.draw(shader);

    GLfloat angle = 2.0f * SDL_GetTicks() * 0.001f;
    angle = cos(angle);

    glm::mat4 lamp2_model_pos;
    lamp2_model_pos = glm::translate(lamp2_model_pos, light_position);
    lamp2_model_pos = glm::scale(lamp2_model_pos, glm::vec3(0.2));
    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(lamp2_model_pos)));
    shader_light.use();
    shader_light.set_mat4("view", view);
    shader_light.set_mat4("projection", projection);
    shader_light.set_mat4("model", lamp2_model_pos);
    shader_light.set_mat3("transpose_model", transpose_model);

    lamp2.draw(shader_light);

    // spotLight
    shader.use();
    shader.set_vec3("spot_light.position", 0.500197f, 3.63321f, -0.0114202f);
    shader.set_vec3("spot_light.direction", angle, -1.0f, -0.03378f);
    shader.set_vec3("spot_light.ambient", 0.0f, 0.0f, 0.0f);
    shader.set_vec3("spot_light.diffuse", 1.0f, 1.0f, 1.0f);
    shader.set_vec3("spot_light.specular", 1.0f, 1.0f, 1.0f);
    shader.set_float("spot_light.constant", 1.0f);
    shader.set_float("spot_light.linear", 0.09);
    shader.set_float("spot_light.quadratic", 0.032);
    shader.set_float("spot_light.cut_off", glm::cos(glm::radians(12.5f)));
    shader.set_float("spot_light.outer_cut_off", glm::cos(glm::radians(15.0f)));

    glm::mat4 lamp_model_pos;
    lamp_model_pos = glm::translate(lamp_model_pos, glm::vec3(.5f, 5.5f, 0.0f));
    lamp_model_pos = glm::scale(lamp_model_pos, glm::vec3(1.0f));
    lamp_model_pos = glm::rotate(lamp_model_pos, angle * 20.0f, glm::vec3(0.0f, 0.0f, 1.0f));

    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(lamp_model_pos)));
    shader.set_mat4("view", view);
    shader.set_mat4("projection", projection);
    shader.set_mat4("model", lamp_model_pos);
    shader.set_mat3("transpose_model", transpose_model);

    lamp.draw(shader);

    glad_glBindVertexArray(0);

    glEnable(GL_FRAMEBUFFER_SRGB);  // Gamma correction
    SDL_GL_SwapWindow(window);
    glDisable(GL_FRAMEBUFFER_SRGB);
  }

  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}

void check_user_input(Camera& camera)
{
  if (keys[SDLK_ESCAPE])
  {
    game_loop = GL_FALSE;
  }
  if (keys[SDLK_w])
  {
    camera.keyboard_input(camera.cam_forward, delta_time);
  }
  if (keys[SDLK_s])
  {
    camera.keyboard_input(camera.cam_backward, delta_time);
  }
  if (keys[SDLK_d])
  {
    camera.keyboard_input(camera.cam_right, delta_time);
  }
  if (keys[SDLK_a])
  {
    camera.keyboard_input(camera.cam_left, delta_time);
  }
  if (keys[SDLK_1])
  {
  }
  if (keys[SDLK_2])
  {
  }
}

void make_texture(GLuint& texture, const char* path)
{
  glad_glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(path, &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glad_glBindTexture(GL_TEXTURE_2D, texture);
    glad_glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glad_glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load at path." << std::endl;
    stbi_image_free(data);
  }
}
