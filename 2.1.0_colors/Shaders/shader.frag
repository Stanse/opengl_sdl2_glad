#version 300 es
precision mediump float;

out vec4 color;

uniform vec3 light_color;
uniform vec3 object_color;

void main()
{
    color = vec4(object_color * light_color, 1.0f);
}
