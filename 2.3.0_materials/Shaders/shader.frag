#version 300 es
precision mediump float;

in vec3 object_normal;
in vec3 fragment_position;

out vec4 color;

uniform vec3 view_pos;

struct Material
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct Light
{
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform Material material;
uniform  Light light;

void main()
{
    // Ambient
    vec3 ambient = light.ambient * material.ambient;

    // Diffuse
    vec3 norm = normalize(object_normal);
    vec3 light_direction = normalize(light.position - fragment_position);

    float diff = max(dot(norm, light_direction), 0.0f);
    vec3 diffuse = light.diffuse * (diff * material.diffuse);

    // Specular
    float specular_strength = 0.5f;
    vec3 view_dir = normalize(view_pos - fragment_position);
    vec3 reflect_dir = reflect(-light_direction, norm);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);
    vec3 specular = light.specular * (spec * material.specular);

    vec3 result = ambient + diffuse + specular;

    color = vec4(result, 1.0f);
}
