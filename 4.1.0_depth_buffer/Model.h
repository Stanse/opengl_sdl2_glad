#ifndef MODEL_H
#define MODEL_H

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.h"
#include "stb_image.h"

GLuint texture_from_file(const GLchar* path, const std::string& directory /*, bool gamma = false*/);

class Model
{
public:
  // data
  std::vector<Texture> textures_loaded;
  std::vector<Mesh> meshes;
  std::string directory;
  bool gamma_corection = false;

  Model(const std::string& path, bool gamma = false) : gamma_corection(gamma)
  {
    load_model(path);
  }

  void draw(Shader shader)
  {
    for (Mesh mesh : meshes)
    {
      mesh.draw(shader);
    }
  }

private:
  void load_model(const std::string& path);
  void process_node(aiNode* node, const aiScene* scene);
  Mesh process_mesh(aiMesh* mesh, const aiScene* scene);
  std::vector<Texture> load_material_textures(aiMaterial* mat, aiTextureType type, std::string type_name);
};

GLuint texture_from_file(const GLchar* path, const std::string& directory /*, bool gamma*/)
{
  std::string filename = std::string(path);
  filename = directory + '/' + filename;

  GLuint texture;
  glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(filename.c_str(), &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load from path: " << filename << std::endl;
    stbi_image_free(data);
  }

  return texture;
}

std::vector<Texture> Model::load_material_textures(aiMaterial* mat, aiTextureType type, std::string type_name)
{
  std::vector<Texture> textures;
  for (GLuint i = 0; i < mat->GetTextureCount(type); ++i)
  {
    aiString str;
    mat->GetTexture(type, i, &str);
    GLboolean skip = false;
    for (GLuint j = 0; j < textures_loaded.size(); ++j)
    {
      if (std::strcmp(textures_loaded[j].path.data(), str.C_Str()) == 0)
      {
        textures.push_back(textures_loaded[j]);
        skip = true;
        break;
      }
    }
    if (!skip)
    {
      Texture texture;
      texture.id = texture_from_file(str.C_Str(), this->directory);
      texture.type = type_name;
      texture.path = str.C_Str();
      textures.push_back(texture);
      textures_loaded.push_back(texture);
    }
  }
  return textures;
}

Mesh Model::process_mesh(aiMesh* mesh, const aiScene* scene)
{
  std::vector<Vertex> vertices;
  std::vector<GLuint> indices;
  std::vector<Texture> textures;

  for (GLuint i = 0; i < mesh->mNumVertices; ++i)
  {
    Vertex vertex;
    glm::vec3 vector;
    // positions
    vector.x = mesh->mVertices[i].x;
    vector.y = mesh->mVertices[i].y;
    vector.z = mesh->mVertices[i].z;
    vertex.position = vector;
    // normals
    vector.x = mesh->mNormals[i].x;
    vector.y = mesh->mNormals[i].y;
    vector.z = mesh->mNormals[i].z;
    vertex.normal = vector;

    if (mesh->mTextureCoords[0])  // does the mesh contain texture coordinates?
    {
      glm::vec2 vec;
      // a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
      // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
      vec.x = mesh->mTextureCoords[0][i].x;
      vec.y = mesh->mTextureCoords[0][i].y;
      vertex.texture_coordinates = vec;
    }
    else
    {
      vertex.texture_coordinates = glm::vec2(0.0f, 0.0f);
    }

    //      // tangent
    //      vector.x = mesh->mTangents[i].x;
    //      vector.y = mesh->mTangents[i].y;
    //      vector.z = mesh->mTangents[i].z;
    //      vertex.Tangent = vector;
    //      // bitangent
    //      vector.x = mesh->mBitangents[i].x;
    //      vector.y = mesh->mBitangents[i].y;
    //      vector.z = mesh->mBitangents[i].z;
    //      vertex.Bitangent = vector;
    vertices.push_back(vertex);
  }

  for (GLuint i = 0; i < mesh->mNumFaces; ++i)
  {
    aiFace face = mesh->mFaces[i];
    for (GLuint j = 0; j < face.mNumIndices; j++)
    {
      indices.push_back(face.mIndices[j]);
    }
  }

  if (mesh->mMaterialIndex >= 0)
  {
    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
    // 1. diffuse maps
    std::vector<Texture> diffuse_maps = load_material_textures(material, aiTextureType_DIFFUSE, "texture_diffuse");
    textures.insert(textures.end(), diffuse_maps.begin(), diffuse_maps.end());
    // 2. specular maps
    std::vector<Texture> specular_maps = load_material_textures(material, aiTextureType_SPECULAR, "texture_specular");
    textures.insert(textures.end(), specular_maps.begin(), specular_maps.end());
    // 3. normal maps
    std::vector<Texture> normal_maps = load_material_textures(material, aiTextureType_NORMALS, "texture_normal");
    textures.insert(textures.end(), normal_maps.begin(), normal_maps.end());

    return Mesh(vertices, indices, textures);
  }
}

void Model::process_node(aiNode* node, const aiScene* scene)
{
  GLuint num_meshes = node->mNumMeshes;
  for (GLuint i = 0; i < num_meshes; ++i)
  {
    GLuint mesh_index = node->mMeshes[i];
    aiMesh* mesh = scene->mMeshes[mesh_index];
    meshes.push_back(process_mesh(mesh, scene));
  }

  GLuint num_children = node->mNumChildren;
  for (GLuint i = 0; i < num_children; ++i)
  {
    process_node(node->mChildren[i], scene);
  }
}

void Model::load_model(const std::string& path)
{
  Assimp::Importer importer;
  const aiScene* scene =
      importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

  if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)  // if is Not Zero
  {
    std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
    return;
  }

  directory = path.substr(0, path.find_last_of('/'));
  process_node(scene->mRootNode, scene);
}

#endif  // MODEL_H
