#include "Shader.h"
#include "Camera.h"
#include <SDL.h>
#include <cassert>

#include "Model.h"

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

const GLint width = 1200;
const GLint height = 1000;
static GLboolean keys[1024]{ false };

GLboolean game_loop = true;
static GLfloat delta_time = 0.0f;
static GLfloat last_frame = 0.0f;
static GLfloat cursor_last_x = 400, cursor_last_y = 300;

static glm::vec3 light_position(1.2f, 1.0f, 2.0f);

void make_texture(GLuint& texture, const char* path);
void check_user_input(Camera& camera);

int main()
{
  const GLint sdl_init_result = SDL_Init(SDL_INIT_EVERYTHING);
  if (sdl_init_result != 0)
  {
    const char* err_msg{ SDL_GetError() };
    std::cerr << "ERROR: Failed call SDL_Init:\n" << err_msg << std::endl;
    return -1;
  }

  SDL_Window* window = SDL_CreateWindow("OpenGL ES 3.2 || 4.1.0 Depth buffer", SDL_WINDOWPOS_CENTERED,
                                        SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
  if (!window)
  {
    const char* err_msg{ SDL_GetError() };
    std::cerr << "ERROR: Failed call SDL_CreateWindow" << err_msg << std::endl;
    return -1;
  }

  GLint gl_major_ver = 3;
  GLint gl_minor_ver = 2;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);
  SDL_GLContext context{ SDL_GL_CreateContext(window) };

  assert(context != nullptr);

  GLint result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
  assert(result == 0);
  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
  assert(result == 0);

  if (gl_major_ver < 3 && gl_minor_ver < 3)
  {
    std::clog << "current contex openGL version: " << gl_major_ver << '.' << gl_minor_ver << '\n' << std::flush;
    throw std::runtime_error("openGL version too low");
  }

  if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
  {
    std::clog << "ERROR: failed to initialize GLAD" << std::endl;
  }

  Shader shader("Shaders/shader.vs", "Shaders/shader.frag");
  Shader light_shader("Shaders/light_shader.vs", "Shaders/light_shader.frag");

  SDL_Event event;
  Camera camera;
  camera.position = glm::vec3(3.9f, 2.12f, 7.94f);
  camera.yaw = -88;
  camera.pitch = -13.25;
  camera.mouse_input(0, 0);

  Model cube("../models/cube/cube.obj");
  Model floor("../models/floor/floor.obj");

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  //      +-------------------------+
  //      |         Game loop       |
  //      +-------------------------+
  while (game_loop)
  {
    GLfloat current_frame = SDL_GetTicks() * 0.001;
    delta_time = current_frame - last_frame;
    last_frame = current_frame;

    //      | INPUT EVENT
    //      +-------------------------+
    while (SDL_PollEvent(&event) != 0)
    {
      if (event.type == SDL_QUIT)
        game_loop = GL_FALSE;
      //  KEYBOARD
      if (event.type == SDL_KEYDOWN)
      {
        if (event.key.keysym.sym >= 0 && event.key.keysym.sym <= 1024)
        {
          keys[event.key.keysym.sym] = GL_TRUE;
        }
      }
      if (event.type == SDL_KEYUP)
      {
        if (event.key.keysym.sym >= 0 && event.key.keysym.sym <= 1024)
        {
          keys[event.key.keysym.sym] = GL_FALSE;
        }
      }
      // MOUSE MOUTION
      if (event.type == SDL_MOUSEMOTION)
      {
        GLint cursor_x_pos;
        GLint cursor_y_pos;
        SDL_GetMouseState(&cursor_x_pos, &cursor_y_pos);

        GLfloat xoffset = cursor_x_pos - cursor_last_x;
        GLfloat yoffset = -cursor_y_pos + cursor_last_y;  // Reversed since y-coordinates go
                                                          // from bottom to left
        cursor_last_x = cursor_x_pos;
        cursor_last_y = cursor_y_pos;

        camera.mouse_input(xoffset, yoffset);
      }
      // MOUSE WHEEL
      if (event.type == SDL_MOUSEWHEEL)
      {
        GLfloat offset = 1;
        if (event.wheel.y > 0)  // scroll up
        {
          if (camera.zoom < 45)
          {
            camera.zoom += offset;
          }
          else
          {
            camera.zoom = 45;
          }
        }
        else if (event.wheel.y < 0)  // scroll down
        {
          if (camera.zoom > 1)
          {
            camera.zoom -= offset;
          }
          else
          {
            camera.zoom = 1;
          }
        }
      }
    }

    glad_glClearColor(.24f, .25f, .25f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    check_user_input(camera);

    //      +-------------------------+
    //      |    CUBE SHADER USE      |
    //      +-------------------------+
    shader.use();

    //    +-------------------------+
    //    |   LIGHT SETTINGS        |
    //    +-------------------------+
    shader.set_float("material.shininess", 64.0f);
    // directional light
    shader.set_vec3("direct_light.direction", -0.2f, -1.0f, -0.3f);
    shader.set_vec3("direct_light.ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("direct_light.diffuse", 0.8f, 0.8f, 0.8f);  // darken the light a bit to fit the scene
    shader.set_vec3("direct_light.specular", 0.5f, 0.5f, 0.5f);

    //    +-------------------------+
    //    |   VIEW                  |
    //    +-------------------------+
    glm::mat4 view{ camera.get_look_at_matrix() };
    GLint view_loc = glGetUniformLocation(shader.program, "view");
    glUniformMatrix4fv(view_loc, 1, GL_FALSE, glm::value_ptr(view));

    shader.set_vec3("view_pos", camera.position.x, camera.position.y, camera.position.z);

    //    +-------------------------+
    //    |   PROJECTION            |
    //    +-------------------------+
    glm::mat4 projection;
    projection = glm::perspective(camera.zoom, static_cast<GLfloat>(width / height), 0.1f, 1000.0f);

    GLint proj_loc = glGetUniformLocation(shader.program, "projection");
    glUniformMatrix4fv(proj_loc, 1, GL_FALSE, glm::value_ptr(projection));

    //    +-------------------------+
    //    |   MODEL                 |
    //    +-------------------------+
    GLint model_loc = glGetUniformLocation(shader.program, "model");
    GLint transpose_model_loc = glGetUniformLocation(shader.program, "transpose_model");

    glm::mat4 cube_model_pos;
    cube_model_pos = glm::translate(cube_model_pos, glm::vec3(4.0f, 1.5f, 0.0f));

    glm::mat3 transpose_model;
    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(cube_model_pos)));

    glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(cube_model_pos));
    glUniformMatrix3fv(transpose_model_loc, 1, GL_FALSE, glm::value_ptr(transpose_model));

    cube.draw(shader);

    cube_model_pos = glm::translate(cube_model_pos, glm::vec3(1.2f, 0.0f, -2.3f));
    glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(cube_model_pos));

    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(cube_model_pos)));
    glUniformMatrix3fv(transpose_model_loc, 1, GL_FALSE, glm::value_ptr(transpose_model));
    cube.draw(shader);

    glm::mat4 floor_model_pos;
    glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(floor_model_pos));

    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(floor_model_pos)));
    glUniformMatrix3fv(transpose_model_loc, 1, GL_FALSE, glm::value_ptr(transpose_model));

    floor.draw(shader);

    glad_glBindVertexArray(0);

    SDL_GL_SwapWindow(window);
  }

  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}

void check_user_input(Camera& camera)
{
  if (keys[SDLK_ESCAPE])
  {
    game_loop = GL_FALSE;
  }
  if (keys[SDLK_w])
  {
    camera.keyboard_input(camera.cam_forward, delta_time);
  }
  if (keys[SDLK_s])
  {
    camera.keyboard_input(camera.cam_backward, delta_time);
  }
  if (keys[SDLK_d])
  {
    camera.keyboard_input(camera.cam_right, delta_time);
  }
  if (keys[SDLK_a])
  {
    camera.keyboard_input(camera.cam_left, delta_time);
  }
  if (keys[SDLK_1])
  {
  }
  if (keys[SDLK_2])
  {
  }
}

void make_texture(GLuint& texture, const char* path)
{
  glad_glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(path, &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glad_glBindTexture(GL_TEXTURE_2D, texture);
    glad_glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glad_glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load at path." << std::endl;
    stbi_image_free(data);
  }
}
