#version 300 es
precision mediump float;
in vec3 our_color;
out vec4 pixel_color;

void main()
{
    pixel_color = vec4(our_color, 1.0f);
}
