#include <SDL.h>
#include <cassert>
#include "Shader.h"

membuf load_file(std::string_view path);
std::stringstream filter_comments(std::string_view file);

GLint main(int /*argc*/, char* /*argv*/[])
{
  SDL_version compiled = { 0, 0, 0 };
  SDL_version linked = { 0, 0, 0 };

  SDL_VERSION(&compiled);
  SDL_GetVersion(&linked);

  if (SDL_COMPILEDVERSION != SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
  {
    std::cerr << "warning: SDL2 compiled and linked version mismatch: \n"
              << &compiled << " || " << &linked << std::endl;
  }

  const int sdl_init_result = SDL_Init(SDL_INIT_EVERYTHING);
  if (sdl_init_result != 0)
  {
    const char* err_msg = SDL_GetError();
    std::cerr << "ERROR: failed call SDL_Init: \n" << err_msg << std::endl;
    return -1;
  }

  SDL_Window* window = SDL_CreateWindow("OpenGL ES 3.2 || 1.5.0 Shaders", SDL_WINDOWPOS_CENTERED,
                                        SDL_WINDOWPOS_CENTERED, 540, 960, ::SDL_WINDOW_OPENGL);
  if (window == nullptr)
  {
    const char* err_msg = SDL_GetError();
    std::cerr << "ERROR: failed call SDL_CreateWindow:\n" << err_msg << std::endl;
    SDL_Quit();
    return -1;
  }

  GLint gl_major_ver = 3;
  GLint gl_minor_ver = 0;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
      SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
      gl_context = SDL_GL_CreateContext(window);
    }
  assert(gl_context != nullptr);

  GLint result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
  assert(result == 0);
  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
  assert(result == 0);

  if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
  {
    std::clog << "ERROR: failed to initialize GLAD" << std::endl;
  }

  //Shader simple_shader("res/Shaders/shader.vs", "res/Shaders/shader.frag");
  std::stringstream str_vertex_stream = filter_comments("res/Shaders/shader.vs");
  std::stringstream str_fragment_stream = filter_comments("res/Shaders/shader.frag");
  Shader simple_shader(str_vertex_stream, str_fragment_stream);

  // Set up vertex data (and buffer(s)) and attribute pointers
  GLfloat vertices[] = {
    // Positions         // Colors
    0.5f,  -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,  // Bottom Right
    -0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f,  // Bottom Left
    0.0f,  0.5f,  0.0f, 0.0f, 0.0f, 1.0f   // Top
  };

  GLuint VAO;
  GLuint VBO;

  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);

  glBindVertexArray(VAO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
  // Position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(1);

  glBindVertexArray(0);  // unbind VAO

  bool run = true;
  SDL_Event event;
  while (run)
  {
    while (SDL_PollEvent(&event) != 0)
    {
      if (event.type == SDL_QUIT)
        run = false;
      if (event.type == SDL_KEYDOWN)
      {
        if (event.key.keysym.sym == SDLK_ESCAPE)
          run = false;
      }
    }
    glad_glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glad_glClear(GL_COLOR_BUFFER_BIT);

    simple_shader.use();
    glad_glBindVertexArray(VAO);
    glad_glDrawArrays(GL_TRIANGLES, 0, 3);
    glad_glBindVertexArray(0);

    SDL_GL_SwapWindow(window);
  }

  SDL_GL_DeleteContext(gl_context);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}

membuf load_file(std::string_view path)
{
    SDL_RWops* io = SDL_RWFromFile(path.data(), "rb");
    if (nullptr == io)
    {
        throw std::runtime_error("can't load file: " + std::string(path));
    }

    Sint64 file_size = io->size(io);
    if (-1 == file_size)
    {
        throw std::runtime_error("can't determine size of file: " +
                                 std::string(path));
    }
    size_t                  size = static_cast<size_t>(file_size);
    std::unique_ptr<char[]> mem  = std::make_unique<char[]>(size);

    size_t num_readed_objects = io->read(io, mem.get(), size, 1);
    if (num_readed_objects != 1)
    {
        throw std::runtime_error("can't read all content from file: " +
                                 std::string(path));
    }

    if (0 != io->close(io))
    {
        throw std::runtime_error("failed close file: " + std::string(path));
    }
    return membuf(std::move(mem), size);
}

std::stringstream filter_comments(std::string_view file)
{
    std::stringstream out;
    std::string       line;
    //std::ifstream     in(file.data(), std::ios_base::binary);
    membuf buf = load_file(file);
    std::istream in(&buf);

    if (!in)
    {
        throw std::runtime_error(std::string("can't open file: ") +
                                 file.data());
    }

    while (std::getline(in, line))
    {
        size_t comment_pos = line.find("//");
        if (comment_pos != std::string::npos)
        {
            line = line.substr(0, comment_pos);
        }
        if (!line.empty())
        {
            out << line << '\n';
        }
    }

    return out;
}
