#version 300 es
precision mediump float;
out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D texture_1;
uniform sampler2D texture_2;
uniform float blend;

void main()
{
    FragColor = mix(texture(texture_1, TexCoord), texture(texture_2, TexCoord), blend);
}
