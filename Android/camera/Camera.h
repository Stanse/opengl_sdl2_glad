#pragma once

#include <vector>

#include <iostream>
#include <SDL.h>
#include "../../glad/glad.h"
#include "../../glm/glm.hpp"
#include "../../glm/gtc/matrix_transform.hpp"

// default camera values
const GLfloat d_yaw = -90.0f;
const GLfloat d_pitch = 0.0f;
const GLfloat d_speed = 3.0f;
const GLfloat d_sensitivity = 0.25f;
const GLfloat d_zoom = 45.0f;

class Camera
{
public:
  // Camera Attributes
  glm::vec3 position;
  glm::vec3 front;
  glm::vec3 up;
  glm::vec3 right;
  glm::vec3 world_up;
  // Eular Angles
  GLfloat yaw;
  GLfloat pitch;
  // Camera options
  GLfloat movement_speed;
  GLfloat mouse_sensitivity;
  GLfloat zoom;

  enum camera_movement
  {
    cam_forward,
    cam_backward,
    cam_left,
    cam_right
  };

  Camera(glm::vec3 a_position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 a_world_up = glm::vec3(0.0f, 1.0f, 0.0f),
         GLfloat a_yaw = d_yaw, GLfloat a_pitch = d_pitch)
    : position{ a_position }
    , front{ glm::vec3(0.0f, 0.0f, -1.0f) }
    , world_up{ a_world_up }
    , yaw{ a_yaw }
    , pitch{ a_pitch }
    , movement_speed{ d_speed }
    , mouse_sensitivity{ d_sensitivity }
    , zoom{ d_zoom }
  {
    update_vectors();
  }

  Camera(GLfloat pos_x, GLfloat pos_y, GLfloat pos_z, GLfloat up_x, GLfloat up_y, GLfloat up_z, GLfloat a_yaw,
         GLfloat a_pitch)
    : position{ glm::vec3(pos_x, pos_y, pos_z) }
    , front{ glm::vec3(0.0f, 0.0f, -1.0f) }
    , world_up{ glm::vec3(up_x, up_y, up_z) }
    , yaw{ a_yaw }
    , pitch{ a_pitch }
    , movement_speed{ d_speed }
    , mouse_sensitivity{ d_sensitivity }
    , zoom{ d_zoom }
  {
    update_vectors();
  }

  glm::mat4 get_look_at_matrix()
  {
    return glm::lookAt(position, position + front, up);
  }

  void keyboard_input(camera_movement direction, GLfloat delta_time)
  {
    GLfloat velocity = movement_speed * delta_time;
    if (direction == cam_forward)
    {
      position += front * velocity;
    }
    if (direction == cam_backward)
    {
      position -= front * velocity;
    }
    if (direction == cam_right)
    {
      position += right * velocity;
    }
    if (direction == cam_left)
    {
      position -= right * velocity;
    }
    //    position.y = 0.0f;  // <-- this one-liner keeps the user at the ground level (xz plane)
  }

  void mouse_input(GLfloat xoffset, GLfloat yoffset, GLboolean constrain_pitch = true)
  {
    xoffset *= mouse_sensitivity;
    yoffset *= mouse_sensitivity;

    yaw += xoffset;
    pitch += yoffset;

    if (constrain_pitch)
    {
      if (pitch > 89.0f)
        pitch = 89.0f;
      if (pitch < -89.0f)
        pitch = -89.0f;
    }
    update_vectors();
  }

  void check_mouse_scroll(GLfloat yoffset)
  {
    if (zoom >= 1.0f && zoom <= 45.0f)
      zoom -= yoffset;
    if (zoom <= 1.0f)
      zoom = 1.0f;
    if (zoom >= 45.0f)
      zoom = 45.0f;
  }

private:
  void update_vectors()
  {
    glm::vec3 temp_front;
    temp_front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    temp_front.y = sin(glm::radians(pitch));
    temp_front.z = sin(glm::radians(yaw) * cos(glm::radians(pitch)));
    front = glm::normalize(temp_front);

    right = glm::normalize(glm::cross(front, world_up));
    up = glm::normalize(glm::cross(right, front));
  }
};
