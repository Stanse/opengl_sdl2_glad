#include "Shader.h"

Shader::Shader(const GLchar* vertex_path, const GLchar* fragment_path)
{
  // 1. Retrieve the vertex/fragment source code from filePath
  std::string vertexCode;
  std::string fragmentCode;
  std::ifstream vShaderFile;
  std::ifstream fShaderFile;
  // ensures ifstream objects can throw exceptions:
  vShaderFile.exceptions(std::ifstream::badbit);
  fShaderFile.exceptions(std::ifstream::badbit);
  try
  {
    // Open files
    vShaderFile.open(vertex_path);
    fShaderFile.open(fragment_path);
    std::stringstream vShaderStream, fShaderStream;
    // Read file's buffer contents into streams
    vShaderStream << vShaderFile.rdbuf();
    fShaderStream << fShaderFile.rdbuf();
    // close file handlers
    vShaderFile.close();
    fShaderFile.close();
    // Convert stream into string
    vertexCode = vShaderStream.str();
    fragmentCode = fShaderStream.str();
  }
  catch (std::ifstream::failure e)
  {
    std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
  }
  const GLchar* vertex_src = vertexCode.c_str();
  const GLchar* fragment_src = fragmentCode.c_str();
  //  +-----------------------------+
  //  |         VERTEX  SHADER      |
  //  +-----------------------------+
  GLint vertex_shader = glad_glCreateShader(GL_VERTEX_SHADER);
  glad_glShaderSource(vertex_shader, 1, &vertex_src, nullptr);
  glad_glCompileShader(vertex_shader);
  std::cout << "src code:\n";
  std::cout << vertex_src;

  GLint compiled_status = 0;
  glad_glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compiled_status);
  if (!compiled_status)
  {
    GLint info_len = 0;
    glad_glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &info_len);
    std::vector<char> compile_info(static_cast<size_t>(info_len));
    glad_glGetShaderInfoLog(vertex_shader, info_len, nullptr, compile_info.data());  //!!!!!!!!!
    glad_glDeleteShader(vertex_shader);

    std::cerr << "ERROR: Vertex shader compiled failed:\n" << compile_info.data() << std::endl;
  }

  //  +-----------------------------+
  //  |       FRAGMENT  SHADER      |
  //  +-----------------------------+
  GLint fragment_shader = glad_glCreateShader(GL_FRAGMENT_SHADER);
  glad_glShaderSource(fragment_shader, 1, &fragment_src, nullptr);
  glad_glCompileShader(fragment_shader);

  glad_glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled_status);
  if (!compiled_status)
  {
    GLint info_len = 0;
    std::vector<char> compile_info(static_cast<size_t>(info_len));
    glad_glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &info_len);
    glad_glGetShaderInfoLog(fragment_shader, info_len, nullptr, compile_info.data());
    glad_glDeleteShader(fragment_shader);

    std::cerr << "ERROR: Fragment shader compiled failed:\n" << compile_info.data() << std::endl;
  }

  //  +-----------------------------+
  //  |       SHADER PROGRAM        |
  //  +-----------------------------+
  shader_program = glad_glCreateProgram();
  glad_glAttachShader(shader_program, vertex_shader);
  glad_glAttachShader(shader_program, fragment_shader);
  glad_glLinkProgram(shader_program);

  GLint linked_status = 0;
  glad_glGetProgramiv(shader_program, GL_LINK_STATUS, &linked_status);
  if (!linked_status)
  {
    GLint info_len = 0;
    glad_glGetProgramiv(shader_program, GL_INFO_LOG_LENGTH, &info_len);
    std::vector<char> info_log(static_cast<size_t>(info_len));
    glad_glDeleteProgram(shader_program);

    std::cerr << "Shader program linked failed:\n" << info_log.data() << std::endl;
  }

  glad_glDeleteShader(vertex_shader);
  glad_glDeleteShader(fragment_shader);
}

void Shader::use()
{
  glad_glUseProgram(shader_program);
}
