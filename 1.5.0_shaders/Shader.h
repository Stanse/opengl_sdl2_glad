#ifndef SHADER_H
#define SHADER_H

#include <../glad/glad.h>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

class Shader
{
public:
  GLint shader_program = 0;
  Shader(const GLchar* vertex_path, const GLchar* fragment_path);
  void use();
};

#endif
