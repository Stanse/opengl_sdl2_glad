#include <iostream>
#include "../glad/glad.h"
#include <cassert>
#include <vector>
#include <string_view>
#include <SDL.h>

const GLchar* vertex_shader_src = R"(
                                  #version 330 core
                                  layout (location = 0) in vec3 aPos;
                                  void main()
                                  {
                                     gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
                                  }
                                  )";

const GLchar* vertex_shader_src2 = R"(
                                  #version 330 core
                                  layout (location = 0) in vec3 aPos;
                                  layout (location = 1) in vec3 color;
                                  out vec3 our_color;                       // color for fragment shader
                                  void main()
                                  {
                                    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
                                    our_color = color;
                                  }
                                  )";

const GLchar* fragment_shader_src = R"(
                                    #version 330 core
                                    precision mediump float;
                                    out vec4 FragColor;
                                    void main()
                                    {
                                       FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
                                    }
                                    )";
const GLchar* fragment_shader_src2 = R"(
                                    #version 330 core
                                    precision mediump float;
                                    in vec3 our_color;
                                    out vec4 pixel_color;

                                    void main()
                                    {
                                       pixel_color = vec4(our_color, 1.0f);
                                    }
                                    )";

GLint main()
{
  SDL_version compiled = { 0, 0, 0 };
  SDL_version linked = { 0, 0, 0 };

  SDL_VERSION(&compiled);
  SDL_GetVersion(&linked);

  if (SDL_COMPILEDVERSION != SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
  {
    std::cerr << "warning: SDL2 compiled and linked version mismatch: \n"
              << &compiled << " || " << &linked << std::endl;
  }

  const int sdl_init_result = SDL_Init(SDL_INIT_EVERYTHING);
  if (sdl_init_result != 0)
  {
    const char* err_msg = SDL_GetError();
    std::cerr << "ERROR: failed call SDL_Init: \n" << err_msg << std::endl;
    return -1;
  }

  SDL_Window* window = SDL_CreateWindow("OpenGL ES 3.2 || 1.4.1 Two triangles", SDL_WINDOWPOS_CENTERED,
                                        SDL_WINDOWPOS_CENTERED, 640, 480, ::SDL_WINDOW_OPENGL);
  if (window == nullptr)
  {
    const char* err_msg = SDL_GetError();
    std::cerr << "ERROR: failed call SDL_CreateWindow:\n" << err_msg << std::endl;
    SDL_Quit();
    return -1;
  }

  GLint gl_major_ver = 3;
  GLint gl_minor_ver = 3;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  if (gl_context == nullptr)
  {
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    gl_context = SDL_GL_CreateContext(window);
  }
  assert(gl_context != nullptr);

  GLint result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
  assert(result == 0);
  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
  assert(result == 0);

  if (gl_major_ver < 3 && gl_minor_ver < 3)
  {
    std::clog << "current context openGL version: " << gl_major_ver << '.' << gl_minor_ver << '\n' << std::flush;
    // throw std::runtime_error("openGL version too low");
  }

  if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
  {
    std::clog << "ERROR: failed to initialize GLAD" << std::endl;
  }

  GLint nrAttributes;
  glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
  std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;

  //  +-----------------------------+
  //  |         VERTEX  SHADER      |
  //  +-----------------------------+
  GLint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glad_glShaderSource(vertex_shader, 1, &vertex_shader_src, nullptr);
  glad_glCompileShader(vertex_shader);

  GLint shader_status = 0;
  char info_log[512];
  glad_glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &shader_status);
  if (!shader_status)
  {
    glad_glGetShaderInfoLog(vertex_shader, 512, nullptr, info_log);
    std::cout << "ERROR: Vertex shader compilation failed:\n" << info_log << std::endl;
  }

  GLint vertex_shader2 = glCreateShader(GL_VERTEX_SHADER);
  glad_glShaderSource(vertex_shader2, 1, &vertex_shader_src2, nullptr);
  glad_glCompileShader(vertex_shader2);

  //  +-----------------------------+
  //  |       FRAGMENT  SHADER  1   |
  //  +-----------------------------+
  GLint fragment_shader = glad_glCreateShader(GL_FRAGMENT_SHADER);
  glad_glShaderSource(fragment_shader, 1, &fragment_shader_src, nullptr);
  glad_glCompileShader(fragment_shader);

  glad_glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &shader_status);
  if (!shader_status)
  {
    glad_glGetShaderInfoLog(fragment_shader, 512, nullptr, info_log);
    std::cout << "ERROR: Fragment shader compilation failed:\n" << info_log << std::endl;
  }

  //  +-----------------------------+
  //  |       FRAGMENT  SHADER  2   |
  //  +-----------------------------+
  GLint fragment_shader2 = glad_glCreateShader(GL_FRAGMENT_SHADER);
  glad_glShaderSource(fragment_shader2, 1, &fragment_shader_src2, nullptr);
  glad_glCompileShader(fragment_shader2);

  glad_glGetShaderiv(fragment_shader2, GL_COMPILE_STATUS, &shader_status);
  if (!shader_status)
  {
    glad_glGetShaderInfoLog(fragment_shader2, 512, nullptr, info_log);
    std::cout << "ERROR: Fragment shader compilation failed:\n" << info_log << std::endl;
  }

  //  +-----------------------------+
  //  |       SHADER PROGRAM 1      |
  //  +-----------------------------+

  GLint shader_program = glad_glCreateProgram();
  glad_glAttachShader(shader_program, vertex_shader);
  glad_glAttachShader(shader_program, fragment_shader);
  glad_glLinkProgram(shader_program);

  glad_glGetProgramiv(shader_program, GL_LINK_STATUS, &shader_status);
  if (!shader_status)
  {
    glad_glGetProgramInfoLog(shader_program, 512, nullptr, info_log);
    std::cout << "ERROR: Shader programm linking failed:\n" << info_log << std::endl;
  }

  //  +-----------------------------+
  //  |       SHADER PROGRAM 2      |
  //  +-----------------------------+

  GLint shader_program2 = glad_glCreateProgram();
  glad_glAttachShader(shader_program2, vertex_shader2);
  glad_glAttachShader(shader_program2, fragment_shader2);
  glad_glLinkProgram(shader_program2);

  glad_glDeleteShader(vertex_shader);
  glad_glDeleteShader(vertex_shader2);
  glad_glDeleteShader(fragment_shader);
  glad_glDeleteShader(fragment_shader2);

  // Указывание вершин (и буферов) и настройка вершинных атрибутов
  GLfloat vertices[] = {
    -1.0f, -0.5f, 0.0f,  // левая вершина
    0.0f,  -0.5f, 0.0f,  // правая вершина
    -0.5f, 0.5f,  0.0f   // верхняя вершина
  };

  GLfloat vertices2[] = {
    // Позиции         // Цвета
    0.5f,  -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,  // Нижний правый угол
    -0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f,  // Нижний левый угол
    0.0f,  0.5f,  0.0f, 0.0f, 0.0f, 1.0f   // Верхний угол
  };

  GLuint VAO;
  GLuint VBO;

  GLuint VAO2;
  GLuint VBO2;

  glad_glGenVertexArrays(1, &VAO);
  glad_glGenBuffers(1, &VBO);

  glad_glBindVertexArray(VAO);
  glad_glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glad_glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glad_glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);

  glad_glEnableVertexAttribArray(0);
  glad_glBindBuffer(GL_ARRAY_BUFFER, 0);
  glad_glBindVertexArray(0);

  glad_glGenVertexArrays(1, &VAO2);
  glad_glGenBuffers(1, &VBO2);

  glad_glBindVertexArray(VAO2);
  glad_glBindBuffer(GL_ARRAY_BUFFER, VBO2);
  glad_glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW);
  // Атрибут с координатами
  glad_glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
  glad_glEnableVertexAttribArray(0);
  // Атрибут с цветом
  glad_glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glad_glEnableVertexAttribArray(1);

  glad_glBindVertexArray(0);
  glad_glBindBuffer(GL_ARRAY_BUFFER, 0);
  //  +-----------------------------+
  //  |           RENDER            |
  //  +-----------------------------+
  bool run = true;
  SDL_Event event;
  while (run)
  {
    while (SDL_PollEvent(&event) != 0)
    {
      if (event.type == SDL_QUIT)
        run = false;
      if (event.type == SDL_KEYDOWN)
      {
        if (event.key.keysym.sym == SDLK_ESCAPE)
          run = false;
      }
    }
    glad_glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glad_glClear(GL_COLOR_BUFFER_BIT);

    glad_glUseProgram(shader_program);
    glad_glBindVertexArray(VAO);
    glad_glDrawArrays(GL_TRIANGLES, 0, 3);
    glad_glBindVertexArray(0);

    glad_glUseProgram(shader_program2);
    GLfloat time_value = SDL_GetTicks() / 1000.0f;
    GLfloat green_value = (sin(time_value) / 2) + 0.5f;
    GLint vertex_color_location = glad_glGetUniformLocation(shader_program2, "our_color");
    glUniform4f(vertex_color_location, 0.0f, green_value, 0.0f, 1.0f);
    glad_glBindVertexArray(VAO2);
    glad_glDrawArrays(GL_TRIANGLES, 0, 3);
    glad_glBindVertexArray(0);

    SDL_GL_SwapWindow(window);
  }

  SDL_GL_DeleteContext(gl_context);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}
