cmake_minimum_required(VERSION 3.16)

project(4.5.0_frame_buffer LANGUAGES CXX C)

add_executable(4.5.0_frame_buffer
	main.cpp
	../glad/glad.c
        Shader.h
        Camera.h
	)

target_compile_features(4.5.0_frame_buffer PUBLIC cxx_std_17)

find_package(sdl2 REQUIRED)
find_package(assimp REQUIRED)

target_include_directories(4.5.0_frame_buffer PUBLIC
		${SDL2_INCLUDE_DIRS}
		${CMAKE_CURRENT_LIST_DIR}
                ${ASSIMP_INCLUDE_DIRS}
                )

target_link_libraries(4.5.0_frame_buffer PUBLIC
		-lSDL2
		-lGL
		-ldl
                -lassimp
		)
