#ifndef MESH_H
#define MESH_H

#include "Shader.h"

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"

struct Vertex
{
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec2 texture_coordinates;

  //  glm::vec3 tangent;
  //  glm::vec3 bitangent;
};

struct Texture
{
  GLuint id;
  std::string type;
  std::string path;
};

class Mesh
{
public:
  std::vector<Vertex> vertices;
  std::vector<GLuint> indices;
  std::vector<Texture> textures;
  GLuint VAO;

  Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures)
  {
    this->vertices = vertices;
    this->indices = indices;
    this->textures = textures;

    setup_mesh();
  }

  void draw(Shader shader)
  {
    GLuint diffuse_n = 1;
    GLuint specular_n = 1;
    GLuint normal_n = 1;
    GLuint height_n = 1;

    for (GLuint i = 0; i < textures.size(); ++i)
    {
      glActiveTexture(GL_TEXTURE0 + i);
      std::string number;
      std::string name = textures[i].type;
      if (name == "texture_diffuse")
      {
        number = std::to_string(diffuse_n++);
      }
      else if (name == "texture_specular")
      {
        number = std::to_string(specular_n++);
      }
      else if (name == "texture_normal")
      {
        number = std::to_string(normal_n++);
      }
      else if (name == "texture_height")
      {
        number = std::to_string(height_n++);
      }

      const std::string path = ("material." + name + number).c_str();
      //      std::cout << "path to diffuse texture: " << path << '\n';
      shader.set_int(("material." + name + number).c_str(), i);
      GLuint index = textures[i].id;

      glBindTexture(GL_TEXTURE_2D, index);
    }

    glBindVertexArray(VAO);
    glad_glEnable(GL_FRAMEBUFFER_SRGB);  // Gamma correction
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    glad_glDisable(GL_FRAMEBUFFER_SRGB);  // Gamma correction
    glBindVertexArray(0);

    glActiveTexture(GL_TEXTURE0);
  }

private:
  GLuint VBO, EBO;

  void setup_mesh()
  {
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);
    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Normals attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
    glEnableVertexAttribArray(1);
    // Texture coordinates attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texture_coordinates));
    glEnableVertexAttribArray(2);
    //    // tangent attribute
    //    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, tangent));
    //    glEnableVertexAttribArray(3);
    //    // bitangent attribute
    //    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, bitangent));
    //    glEnableVertexAttribArray(4);

    glBindVertexArray(0);
  }
};

#endif  // MESH_H
