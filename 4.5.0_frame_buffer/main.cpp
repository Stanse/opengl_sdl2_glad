#include "Shader.h"
#include "Camera.h"
#include <SDL.h>
#include <cassert>
#include <map>

#include "Model.h"

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

const GLint width = 1200;
const GLint height = 1000;
static GLboolean keys[1024]{ false };
static GLint debug_mode = -1;

GLboolean game_loop = true;
static GLfloat delta_time = 0.0f;
static GLfloat last_frame = 0.0f;
static GLfloat cursor_last_x = 400, cursor_last_y = 300;

static glm::vec3 lamp_position(-3.0f, 2.0f, 0.0f);

void make_texture(GLuint& texture, const char* path);
void check_user_input(Camera& camera);
void print_opengl_version();
GLuint make_custom_frame_buffer(GLuint& textureColorbuffer);

int main()
{
  const GLint sdl_init_result = SDL_Init(SDL_INIT_EVERYTHING);
  if (sdl_init_result != 0)
  {
    const char* err_msg{ SDL_GetError() };
    std::cerr << "ERROR: Failed call SDL_Init:\n" << err_msg << std::endl;
    return -1;
  }

  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);

  SDL_Window* window = SDL_CreateWindow("OpenGL ES 3.2 || 4.5.0 Frame buffer", SDL_WINDOWPOS_CENTERED,
                                        SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
  if (!window)
  {
    const char* err_msg{ SDL_GetError() };
    std::cerr << "ERROR: Failed call SDL_CreateWindow" << err_msg << std::endl;
    return -1;
  }

  GLint gl_major_ver = 3;
  GLint gl_minor_ver = 2;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);
  SDL_GLContext context{ SDL_GL_CreateContext(window) };

  assert(context != nullptr);

  GLint result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
  assert(result == 0);
  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
  assert(result == 0);

  if (gl_major_ver < 3 && gl_minor_ver < 3)
  {
    std::clog << "current contex openGL version: " << gl_major_ver << '.' << gl_minor_ver << '\n' << std::flush;
    throw std::runtime_error("openGL version too low");
  }

  if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
  {
    std::clog << "ERROR: failed to initialize GLAD" << std::endl;
  }
  print_opengl_version();

  Shader shader("Shaders/shader.vs", "Shaders/shader.fs");
  //  Shader shader_single_color("Shaders/single_color.vs", "Shaders/single_color.fs");
  Shader shader_lamp("Shaders/light_shader.vs", "Shaders/light_shader.fs");
  Shader shader_screen("Shaders/frame_buffer.vs", "Shaders/frame_buffer.fs");

  SDL_Event event;
  Camera camera;
  camera.position = glm::vec3(3.9f, 2.12f, 7.94f);
  camera.yaw = -88;
  camera.pitch = -13.25;
  camera.mouse_input(0, 0);
  //      +-------------------------+
  //      |         MODELS          |
  //      +-------------------------+

  Model cube("../models/cube/cube.obj");
  Model floor("../models/floor/floor.obj");
  Model lamp("../models/planet/planet.obj");

  float quadVertices[] = { // vertex attributes for a quad that fills the entire screen in Normalized Device
                           // Coordinates. positions   // texCoords
                           -1.0f, 1.0f, 0.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, -1.0f, 1.0f, 0.0f,

                           -1.0f, 1.0f, 0.0f, 1.0f, 1.0f,  -1.0f, 1.0f, 0.0f, 1.0f, 1.0f,  1.0f, 1.0f
  };

  // screen quad VAO
  unsigned int quadVAO, quadVBO;
  glGenVertexArrays(1, &quadVAO);
  glGenBuffers(1, &quadVBO);
  glBindVertexArray(quadVAO);
  glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

  GLuint textureColorbuffer;
  GLuint framebuffer = make_custom_frame_buffer(textureColorbuffer);
  //      +-------------------------+
  //      |  Global OpenGL settings |
  //      +-------------------------+
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_MULTISAMPLE);  // set MSAA

  glEnable(GL_CULL_FACE);
  //      +-------------------------+
  //      |         Game loop       |
  //      +-------------------------+
  while (game_loop)
  {
    //      | Custom framebuffer
    //      +-------------------------
    glad_glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glEnable(GL_DEPTH_TEST);

    GLfloat current_frame = SDL_GetTicks() * 0.001;
    delta_time = current_frame - last_frame;
    last_frame = current_frame;

    //      | INPUT EVENT
    //      +-------------------------
    while (SDL_PollEvent(&event) != 0)
    {
      if (event.type == SDL_QUIT)
        game_loop = GL_FALSE;
      //  KEYBOARD
      if (event.type == SDL_KEYDOWN)
      {
        if (event.key.keysym.sym == SDLK_F1)
        {
          debug_mode = -debug_mode;
          continue;
        }
        if (event.key.keysym.sym >= 0 && event.key.keysym.sym <= 1024)
        {
          keys[event.key.keysym.sym] = GL_TRUE;
        }
      }
      if (event.type == SDL_KEYUP)
      {
        if (event.key.keysym.sym >= 0 && event.key.keysym.sym <= 1024)
        {
          keys[event.key.keysym.sym] = GL_FALSE;
        }
      }
      // MOUSE MOUTION
      if (event.type == SDL_MOUSEMOTION)
      {
        GLint cursor_x_pos;
        GLint cursor_y_pos;
        SDL_GetMouseState(&cursor_x_pos, &cursor_y_pos);

        GLfloat xoffset = cursor_x_pos - cursor_last_x;
        GLfloat yoffset = -cursor_y_pos + cursor_last_y;  // Reversed since y-coordinates go
                                                          // from bottom to left
        cursor_last_x = cursor_x_pos;
        cursor_last_y = cursor_y_pos;

        camera.mouse_input(xoffset, yoffset);
      }
      // MOUSE WHEEL
      if (event.type == SDL_MOUSEWHEEL)
      {
        GLfloat offset = 1;
        if (event.wheel.y > 0)  // scroll up
        {
          if (camera.zoom < 45)
          {
            camera.zoom += offset;
          }
          else
          {
            camera.zoom = 45;
          }
        }
        else if (event.wheel.y < 0)  // scroll down
        {
          if (camera.zoom > 1)
          {
            camera.zoom -= offset;
          }
          else
          {
            camera.zoom = 1;
          }
        }
      }
    }

    glad_glClearColor(0.75f, 0.75f, 0.75f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (debug_mode == 1)
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    check_user_input(camera);

    //    |   VIEW
    //    +-------------------------+
    glm::mat4 view{ camera.get_look_at_matrix() };
    shader.use();
    shader.set_mat4("view", view);
    shader.set_vec3("view_pos", camera.position.x, camera.position.y, camera.position.z);

    //    +-------------------------+
    //    |   PROJECTION            |
    //    +-------------------------+
    glm::mat4 projection;
    shader.use();
    projection = glm::perspective(camera.zoom, static_cast<GLfloat>(width / height), 0.1f, 1000.0f);
    shader.set_mat4("projection", projection);

    //    +-------------------------+
    //    |   MODEL                 |
    //    +-------------------------+
    glm::mat3 transpose_model;
    shader.use();

    glm::mat4 floor_model_pos;
    shader.set_mat4("model", floor_model_pos);
    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(floor_model_pos)));
    shader.set_mat3("transpose_model", transpose_model);
    floor.draw(shader);

    glm::mat4 cube_model_pos;
    cube_model_pos = glm::translate(cube_model_pos, glm::vec3(1.6f, 1.175f, 2.0f));
    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(cube_model_pos)));
    shader.set_mat4("model", cube_model_pos);
    shader.set_mat3("transpose_model", transpose_model);
    cube.draw(shader);

    cube_model_pos = glm::translate(cube_model_pos, glm::vec3(1.2f, 0.0f, -2.3f));
    shader.set_mat4("model", cube_model_pos);
    transpose_model = static_cast<glm::mat3>(glm::transpose(glm::inverse(cube_model_pos)));
    shader.set_mat3("transpose_model", transpose_model);
    cube.draw(shader);

    // LAMP
    glm::mat4 light_model_pos(1.0f);
    light_model_pos = glm::translate(light_model_pos, lamp_position);
    light_model_pos = glm::scale(light_model_pos, glm::vec3(1.0f, 1.35f, 1.0f));
    light_model_pos = glm::scale(light_model_pos, glm::vec3(0.1f));

    shader_lamp.use();
    shader_lamp.set_mat4("projection", projection);
    shader_lamp.set_mat4("view", view);
    shader_lamp.set_mat4("model", light_model_pos);
    lamp.draw(shader_lamp);

    //    +-------------------------+
    //    |   LIGHT SETTINGS        |
    //    +-------------------------+
    shader.use();
    shader.set_float("material.shininess", 16.0f);
    // directional light
    shader.set_vec3("direct_light.direction", -0.2f, -0.5f, -0.3f);
    shader.set_vec3("direct_light.ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("direct_light.diffuse", 0.4f, 0.4f, 0.4f);  // darken the light a bit to fit the scene
    shader.set_vec3("direct_light.specular", 0.5f, 0.5f, 0.5f);

    // Point light
    shader.set_vec3("point_lights[0].position", lamp_position.x, lamp_position.y, lamp_position.z);
    shader.set_vec3("point_lights[0].ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("point_lights[0].diffuse", 0.7f, 0.7f, 0.7f);
    shader.set_vec3("point_lights[0].specular", 0.8f, 0.8f, 0.8f);
    shader.set_float("point_lights[0].constant", 1.0f);
    shader.set_float("point_lights[0].linear", 0.09);
    shader.set_float("point_lights[0].quadratic", 0.032);

    GLfloat t = SDL_GetTicks() * 0.0020;
    lamp_position.x = sin(t) * 5.0f;
    lamp_position.z = cos(t) * 5.0f;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDisable(GL_DEPTH_TEST);
    //    glClear(GL_COLOR_BUFFER_BIT);

    shader_screen.use();
    glad_glBindVertexArray(quadVAO);
    glBindTexture(GL_TEXTURE_2D, textureColorbuffer);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    SDL_GL_SwapWindow(window);
  }

  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}

void check_user_input(Camera& camera)
{
  if (keys[SDLK_ESCAPE])
  {
    game_loop = GL_FALSE;
  }
  if (keys[SDLK_w])
  {
    camera.keyboard_input(camera.cam_forward, delta_time);
  }
  if (keys[SDLK_s])
  {
    camera.keyboard_input(camera.cam_backward, delta_time);
  }
  if (keys[SDLK_d])
  {
    camera.keyboard_input(camera.cam_right, delta_time);
  }
  if (keys[SDLK_a])
  {
    camera.keyboard_input(camera.cam_left, delta_time);
  }
  if (keys[SDLK_1])
  {
  }
  if (keys[SDLK_2])
  {
  }
}

void make_texture(GLuint& texture, const char* path)
{
  glad_glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(path, &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glad_glBindTexture(GL_TEXTURE_2D, texture);
    glad_glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glad_glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load at path." << std::endl;
    stbi_image_free(data);
  }
}

GLuint make_custom_frame_buffer(GLuint& textureColorbuffer)
{
  // framebuffer configuration
  // -------------------------
  GLuint framebuffer;
  glGenFramebuffers(1, &framebuffer);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

  // We will render pixel color to texture
  glGenTextures(1, &textureColorbuffer);
  glBindTexture(GL_TEXTURE_2D, textureColorbuffer);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureColorbuffer, 0);

  // We will render DEPTH AND STENCIL to texture
  GLuint depth_texture;
  glad_glGenTextures(1, &depth_texture);
  glBindTexture(GL_TEXTURE_2D, depth_texture);
  glad_glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8,
                    NULL);
  glad_glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depth_texture, 0);

  //  // Render DEPTH AND STENCIL to Render buffer
  //  GLuint RBO;
  //  glGenRenderbuffers(1, &RBO);
  //  glBindRenderbuffer(GL_RENDERBUFFER, RBO);
  //  // use a single renderbuffer object for both a depth AND stencil buffer.
  //  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
  //  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RBO);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  return framebuffer;
}

void print_opengl_version()
{
  using namespace std;
  string version = reinterpret_cast<const char*>(glGetString(GL_VERSION));
  string vendor_info = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
  string extentions_info = reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
  clog << "OpenGL version: " << version << endl;
  clog << "OpenGL vendor: " << vendor_info << endl;
  // cerr << "Full OpenGL extention list: " << extentions_info << endl;
};
