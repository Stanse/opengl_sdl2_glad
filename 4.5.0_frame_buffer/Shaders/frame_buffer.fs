#version 320 es
precision highp float;

in vec2 texture_coordinate;
uniform sampler2D screen_texture;

out vec4 fragment_color;

void main()
{
    vec3 color = texture(screen_texture, texture_coordinate).rgb;
    fragment_color = vec4(color, 1.0f);
}

