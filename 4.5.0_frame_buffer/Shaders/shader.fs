#version 320 es
precision highp float;

in vec3 object_normal;
in vec3 fragment_position;
in vec2 texture_coordinates;

out vec4 color;

uniform vec3 view_pos;

//    +-------------------------+
//    |   MATERIAL              |
//    +-------------------------+
struct Material
{
    sampler2D texture_diffuse1;
    sampler2D texture_specular1;
    float shininess;
};
uniform Material material;

//    +-------------------------+
//    |   DIRECTION LIGHT       |
//    +-------------------------+
struct Direction_light
{
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform  Direction_light direct_light;
vec3 calc_direct_light(Direction_light light, vec3 normal, vec3 view_dir);

//    +-------------------------+
//    |   POINT LIGHT           |
//    +-------------------------+
struct Point_light
{
    vec3  position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

#define NR_POINT_LIGHTS 1
uniform Point_light point_lights[NR_POINT_LIGHTS];

vec3 calc_point_light(Point_light light, vec3 normal, vec3 fragment_position, vec3 view_dir);

//    +-------------------------+
//    |   SPOT LIGHT            |
//    +-------------------------+
struct Spot_light
{
    vec3 position;
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;

    float cut_off;
    float outer_cut_off;
};

uniform Spot_light spot_light;

vec3 calc_spot_light(Spot_light light, vec3 normal, vec3 fragment_position, vec3 view_dir);

//    +-------------------------+
//    |   MAIN                  |
//    +-------------------------+
void main()
{
    vec3 view_dir = (view_pos - fragment_position);

    // phase 1: directional lighting
    vec3 result = calc_direct_light(direct_light, object_normal, view_dir);

//    // phase 2: point lights
    for(int i = 0; i < NR_POINT_LIGHTS; i++)
    {
        result += calc_point_light(point_lights[i], object_normal, fragment_position, view_dir);
    }

//    // phase 3: spot light
//    result += calc_spot_light(spot_light, normal, fragment_position, view_dir);

    color = vec4(result, 1.0f);

///    color = vec4(texture(material.texture_diffuse1, texture_coordinates));

}

vec3 calc_direct_light(Direction_light light, vec3 normal, vec3 view_dir)
{
    normal = normalize(normal);
    view_dir = normalize(view_dir);
    // Diffuse
    vec3 light_direction = normalize(-light.direction);
    float diff = max(dot(normal, light_direction), 0.1f);

    // Specular
    vec3 reflect_dir = reflect(light.direction, normal);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);

    // combine results
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.texture_diffuse1, texture_coordinates).rgb);
    vec3 ambient = light.ambient * diffuse;
    vec3 specular = light.specular * spec * vec3(texture(material.texture_specular1, texture_coordinates).rgb);

    vec3 result = ambient + diffuse + specular * glm::vec3(0.2, 1.0f, 1.0f);

    return result;
}

vec3 calc_point_light(Point_light light, vec3 normal, vec3 fragment_position, vec3 view_dir)
{
    vec3 light_dir = normalize(light.position - fragment_position);
    normal = normalize(normal);
    view_dir = normalize(view_dir);

    float diff = max(dot(normal, light_dir), 0.0f);

    vec3 reflect_dir = reflect(-light_dir, normal);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0f), material.shininess);

    // attenuation
    float distance = length(light.position - fragment_position);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

    // combine results
    vec3 ambient = light.ambient * vec3(texture(material.texture_diffuse1, texture_coordinates).rgb);
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.texture_diffuse1, texture_coordinates).rgb);
    vec3 specular = light.specular * spec * vec3(texture(material.texture_specular1, texture_coordinates).rgb);
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation * glm::vec3(0.2, 1.0f, 1.0f);
    return (ambient + diffuse + specular);
}

vec3 calc_spot_light(Spot_light light, vec3 normal, vec3 fragment_position, vec3 view_dir)
{
    vec3 light_dir = normalize(light.position - fragment_position);

    float diff = max(dot(normal, light_dir), 0.0f);

    vec3 reflect_dir = reflect(-light_dir, normal);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0f), material.shininess);

    // attenuation
    float distance = length(light.position - fragment_position);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

    // spotlight intensity
    float theta = dot(light_dir, normalize(-light.direction));
    float epsilon = light.cut_off - light.outer_cut_off;
    float intensity = clamp((theta - light.outer_cut_off) / epsilon, 0.0, 1.0);

    // combine results
    vec3 ambient = light.ambient * vec3(texture(material.texture_diffuse1, texture_coordinates));
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.texture_diffuse1, texture_coordinates));
    vec3 specular = light.specular * spec * vec3(texture(material.texture_specular1, texture_coordinates));

    ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;

    return (ambient + diffuse + specular * glm::vec3(0.2, 1.0f, 1.0f));
}
