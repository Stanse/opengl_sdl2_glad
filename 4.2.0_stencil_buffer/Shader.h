#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#include "../glad/glad.h"

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"

class Shader
{
public:
  GLint program;

  Shader(const GLchar* vertex_path, const GLchar* fragment_path)
  {
    std::string vertex_src_code;
    std::string fragment_src_code;
    std::stringstream str_vertex_stream;
    std::stringstream str_fragment_stream;

    std::ifstream v_file_stream(vertex_path);
    if (!v_file_stream)
    {
      throw std::runtime_error("ERROR: Can't find vertex source file.\n");
    }
    str_vertex_stream << v_file_stream.rdbuf();
    v_file_stream.close();
    vertex_src_code = str_vertex_stream.str();

    std::ifstream f_file_stream(fragment_path);
    if (!f_file_stream)
    {
      throw std::runtime_error("ERROR: Can't find fragment source file.\n");
    }
    str_fragment_stream << f_file_stream.rdbuf();
    f_file_stream.close();
    fragment_src_code = str_fragment_stream.str();

    //  +-----------------------------+
    //  |         VERTEX  SHADER      |
    //  +-----------------------------+
    const GLchar* vertex_src = vertex_src_code.c_str();
    GLint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_src, nullptr);
    glCompileShader(vertex_shader);

    GLint compile_success = 0;
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compile_success);
    if (!compile_success)
    {
      GLint info_len = 0;
      glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &info_len);
      std::vector<char> info_log(static_cast<size_t>(info_len));
      glGetShaderInfoLog(vertex_shader, info_len, nullptr, info_log.data());
      glDeleteShader(vertex_shader);
      std::cerr << "ERROR: Vertex shader compiled failed.\n" << vertex_src_code << '\n' << info_log.data() << std::endl;
    }

    //  +-----------------------------+
    //  |       FRAGMENT  SHADER      |
    //  +-----------------------------+
    const GLchar* fragment_src = fragment_src_code.c_str();
    GLint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_src, nullptr);
    glCompileShader(fragment_shader);

    compile_success = 0;
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compile_success);
    if (!compile_success)
    {
      GLint info_len = 0;
      glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &info_len);
      std::vector<char> info_log(static_cast<size_t>(info_len));
      glGetShaderInfoLog(fragment_shader, info_len, nullptr, info_log.data());
      glDeleteShader(fragment_shader);
      std::cerr << "ERROR: Fragment shader compiled failed.\n"
                << fragment_src_code << '\n'
                << info_log.data() << std::endl;
    }

    //  +-----------------------------+
    //  |       SHADER PROGRAM        |
    //  +-----------------------------+
    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    GLint linked_success = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &linked_success);
    if (!linked_success)
    {
      GLint info_len = 0;
      glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_len);
      std::vector<char> info_log(info_len);
      glGetProgramInfoLog(program, info_len, nullptr, info_log.data());
      glDeleteProgram(program);
      std::cerr << "ERROR: Shader program linked failed.\n" << info_log.data() << std::endl;
    }
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
  }

  void use()
  {
    glUseProgram(program);
  }

  void set_bool(const std::string& name, bool value) const;
  void set_int(const std::string& name, int value) const;
  void set_float(const std::string& name, float value) const;

  void set_vec3(const std::string& name, GLfloat x, GLfloat y, GLfloat z)
  {
    GLint vec3_location = glGetUniformLocation(program, name.c_str());
    glad_glUniform3f(vec3_location, x, y, z);
  }

  void set_mat3(const std::string& name, const glm::mat3& mat) const
  {
    glUniformMatrix3fv(glGetUniformLocation(program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
  }
  // ------------------------------------------------------------------------
  void set_mat4(const std::string& name, const glm::mat4& mat) const
  {
    glUniformMatrix4fv(glGetUniformLocation(program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
  }
};

void Shader::set_int(const std::string& name, int value) const
{
  glUniform1i(glGetUniformLocation(program, name.c_str()), value);
}

void Shader::set_float(const std::string& name, float value) const
{
  glUniform1f(glGetUniformLocation(program, name.c_str()), value);
}

void Shader::set_bool(const std::string& name, bool value) const
{
  glUniform1i(glGetUniformLocation(program, name.c_str()), (int)value);
}
