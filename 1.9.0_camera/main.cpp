#include "Shader.h"
#include "Camera.h"
#include <SDL.h>
#include <cassert>

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

const GLint width = 800;
const GLint height = 600;
static GLfloat mix_value = 0.0f;
static GLboolean keys[1024]{ false };

GLboolean game_loop = true;
static GLfloat delta_time = 0.0f;
static GLfloat last_frame = 0.0f;
static GLfloat cursor_last_x = 400, cursor_last_y = 300;

void make_texture(GLuint& texture, const char* path);
void check_user_input(Camera& camera);

int main()
{
  const GLint sdl_init_result = SDL_Init(SDL_INIT_EVERYTHING);
  if (sdl_init_result != 0)
  {
    const char* err_msg{ SDL_GetError() };
    std::cerr << "ERROR: Failed call SDL_Init:\n" << err_msg << std::endl;
    return -1;
  }

  SDL_Window* window = SDL_CreateWindow("OpenGL ES 3.2 || 1.9.0 Camera", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                        width, height, SDL_WINDOW_OPENGL);
  if (!window)
  {
    const char* err_msg{ SDL_GetError() };
    std::cerr << "ERROR: Failed call SDL_CreateWindow" << err_msg << std::endl;
    return -1;
  }

  GLint gl_major_ver = 3;
  GLint gl_minor_ver = 0;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);
  SDL_GLContext context{ SDL_GL_CreateContext(window) };

  assert(context != nullptr);

  GLint result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
  assert(result == 0);
  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
  assert(result == 0);

  if (gl_major_ver < 3 && gl_minor_ver < 3)
  {
    std::clog << "current contex openGL version: " << gl_major_ver << '.' << gl_minor_ver << '\n' << std::flush;
    throw std::runtime_error("openGL version too low");
  }

  if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
  {
    std::clog << "ERROR: failed to initialize GLAD" << std::endl;
  }

  Shader shader("Shaders/shader.vs", "Shaders/shader.frag");

  float vertices[] = {
    -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 0.5f,  -0.5f, -0.5f, 1.0f, 0.0f, 0.5f,  0.5f,  -0.5f, 1.0f, 1.0f,
    0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f, -0.5f, -0.5f, -0.5f, 0.0f, 0.0f,

    -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, 0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    0.5f,  0.5f,  0.5f,  1.0f, 1.0f, -0.5f, 0.5f,  0.5f,  0.0f, 1.0f, -0.5f, -0.5f, 0.5f,  0.0f, 0.0f,

    -0.5f, 0.5f,  0.5f,  1.0f, 0.0f, -0.5f, 0.5f,  -0.5f, 1.0f, 1.0f, -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, -0.5f, 0.5f,  0.5f,  1.0f, 0.0f,

    0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, 0.5f,  -0.5f, -0.5f, 0.0f, 1.0f,
    0.5f,  -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,  -0.5f, 0.5f,  0.0f, 0.0f, 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

    -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,  -0.5f, -0.5f, 1.0f, 1.0f, 0.5f,  -0.5f, 0.5f,  1.0f, 0.0f,
    0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,

    -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f, 0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    0.5f,  0.5f,  0.5f,  1.0f, 0.0f, -0.5f, 0.5f,  0.5f,  0.0f, 0.0f, -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f
  };

  glm::vec3 cubePositions[] = { glm::vec3(0.0f, 0.0f, 0.0f),    glm::vec3(2.0f, 5.0f, -15.0f),
                                glm::vec3(-1.5f, -2.2f, -2.5f), glm::vec3(-3.8f, -2.0f, -12.3f),
                                glm::vec3(2.4f, -0.4f, -3.5f),  glm::vec3(-1.7f, 3.0f, -7.5f),
                                glm::vec3(1.3f, -2.0f, -2.5f),  glm::vec3(1.5f, 2.0f, -2.5f),
                                glm::vec3(1.5f, 0.2f, -1.5f),   glm::vec3(-1.3f, 1.0f, -1.5f) };

  GLuint VBO, VAO;
  glad_glGenVertexArrays(1, &VBO);
  glad_glGenBuffers(1, &VAO);

  glad_glBindVertexArray(VAO);
  glad_glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glad_glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glad_glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
  glad_glEnableVertexAttribArray(0);

  glad_glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glad_glEnableVertexAttribArray(1);

  glad_glBindVertexArray(0);

  // Load and create a texture
  GLuint box, smile;
  make_texture(box, "container.jpg");
  make_texture(smile, "awesomeface.png");

  SDL_Event event;
  Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

  glEnable(GL_DEPTH_TEST);

  while (game_loop)
  {
    GLfloat current_frame = SDL_GetTicks() * 0.001;
    delta_time = current_frame - last_frame;
    last_frame = current_frame;

    while (SDL_PollEvent(&event) != 0)
    {
      if (event.type == SDL_QUIT)
        game_loop = GL_FALSE;
      //  KEYBOARD
      if (event.type == SDL_KEYDOWN)
      {
        if (event.key.keysym.sym >= 0 && event.key.keysym.sym <= 1024)
        {
          keys[event.key.keysym.sym] = GL_TRUE;
        }
      }
      if (event.type == SDL_KEYUP)
      {
        if (event.key.keysym.sym >= 0 && event.key.keysym.sym <= 1024)
        {
          keys[event.key.keysym.sym] = GL_FALSE;
        }
      }
      // MOUSE MOUTION
      if (event.type == SDL_MOUSEMOTION)
      {
        GLint cursor_x_pos;
        GLint cursor_y_pos;
        SDL_GetMouseState(&cursor_x_pos, &cursor_y_pos);

        GLfloat xoffset = cursor_x_pos - cursor_last_x;
        GLfloat yoffset = cursor_y_pos - cursor_last_y;  // Reversed since y-coordinates go
                                                         // from bottom to left
        cursor_last_x = cursor_x_pos;
        cursor_last_y = cursor_y_pos;

        camera.mouse_input(xoffset, yoffset);
      }
      // MOUSE WHEEL
      if (event.type == SDL_MOUSEWHEEL)
      {
        GLfloat offset = 1;
        if (event.wheel.y > 0)  // scroll up
        {
          if (camera.zoom < 45)
          {
            camera.zoom += offset;
          }
          else
          {
            camera.zoom = 45;
          }
        }
        else if (event.wheel.y < 0)  // scroll down
        {
          if (camera.zoom > 1)
          {
            camera.zoom -= offset;
          }
          else
          {
            camera.zoom = 1;
          }
        }
      }
    }

    glad_glClearColor(.24f, .25f, .25f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    check_user_input(camera);
    shader.use();

    glad_glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, box);
    shader.set_int("texture_1", 0);

    glad_glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, smile);
    shader.set_int("texture_2", 1);

    shader.set_float("blend", mix_value);

    glm::mat4 view{ camera.get_look_at_matrix() };
    glm::mat4 projection;
    projection = glm::perspective(camera.zoom, static_cast<GLfloat>(width / height), 0.1f, 1000.0f);

    GLfloat timeInSec = SDL_GetTicks() * 0.001f;
    //    model = glm::rotate(model, timeInSec * 50.0f, glm::vec3(0.5f, 1.0f, 0.0f));

    GLint model_loc = glGetUniformLocation(shader.program, "model");
    GLint view_loc = glGetUniformLocation(shader.program, "view");
    GLint proj_loc = glGetUniformLocation(shader.program, "projection");

    glUniformMatrix4fv(view_loc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(proj_loc, 1, GL_FALSE, glm::value_ptr(projection));

    glad_glBindVertexArray(VAO);
    for (GLuint i = 0; i < 10; ++i)
    {
      glm::mat4 model;
      model = glm::translate(model, cubePositions[i]);
      GLfloat angle = 20.0f * timeInSec * (i + 2);
      model = glm::rotate(model, angle, glm::vec3(1.0f, 0.3f, 0.5f));
      glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(model));
      glad_glDrawArrays(GL_TRIANGLES, 0, 36);
    }
    glad_glBindVertexArray(0);

    SDL_GL_SwapWindow(window);
  }

  glad_glDeleteVertexArrays(1, &VAO);
  glad_glDeleteBuffers(1, &VBO);

  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}

void check_user_input(Camera& camera)
{
  if (keys[SDLK_ESCAPE])
  {
    game_loop = GL_FALSE;
  }
  if (keys[SDLK_w])
  {
    camera.keyboard_input(camera.cam_forward, delta_time);
  }
  if (keys[SDLK_s])
  {
    camera.keyboard_input(camera.cam_backward, delta_time);
  }
  if (keys[SDLK_d])
  {
    camera.keyboard_input(camera.cam_right, delta_time);
  }
  if (keys[SDLK_a])
  {
    camera.keyboard_input(camera.cam_left, delta_time);
  }
  if (keys[SDLK_1])
  {
    mix_value += 0.05f;
    if (mix_value >= 1.0f)
      mix_value = 1.0f;
  }
  if (keys[SDLK_2])
  {
    mix_value -= 0.05f;
    if (mix_value <= 0.0f)
      mix_value = 0.0f;
  }
}

void make_texture(GLuint& texture, const char* path)
{
  glad_glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(path, &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glad_glBindTexture(GL_TEXTURE_2D, texture);
    glad_glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glad_glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load at path." << std::endl;
    stbi_image_free(data);
  }
}
